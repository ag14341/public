# Heading1 prefix ># Hash for each heading<br>
## Heading2
### Heading3
#### Heading4
##### Heading5
###### Heading6

Normal text as its

Hyperlink
[Github](https://www.github.com)
[Github](https://www.github.com "Github home")

_Italic text here_  prefix and post fix with _
**Bold text here**  prefix and post with **

~~Strikethrough text~~ prefix and post fix with ~
`foreach()` code with prefix and postfix with `

```Language
your code
```
Horizontal line
If you want to add a horizontal line in the markdown file then simply add --- or ***
---
***


1. Item 1
2. Item 2
3. Item 3
   * Sub item 1
   * Sub item 3
* Unordered item
   * Sub item 1
      * Sub sub item 1
         * sub Sub sub item 1
* Unordered item
* Unordered item


![imagename](TargetUrl)

Tables
Tables
|Name|Email|Address|    
|----|-----|-------|   
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  
|John|john@example.com|Address1|  


You can indicate emphasis with bold, italic, or strikethrough text in comment fields and .md files.

Style	Syntax	Keyboard shortcut	Example	Output
Bold	** ** or __ __	command/control + b	**This is bold text**	This is bold text
Italic	* * or _ _	command/control + i	*This text is italicized*	This text is italicized
Strikethrough	~~ ~~		~~This was mistaken text~~	This was mistaken text
Bold and nested italic	** ** and _ _		**This text is _extremely_ important**	This text is extremely important
All bold and italic	*** ***		***All this text is important***	All this text is important

> Text that is a quote

Use `git status` to list all new or modified files that haven't yet been committed.

Some basic Git commands are:
```
git status
git add
git commit
```

[Contribution guidelines for this project](docs/CONTRIBUTING.md)

- George Washington
- John Adams
- Thomas Jefferson

1. James Madison
2. James Monroe
3. John Quincy Adams

1. First list item
   - First nested list item
     - Second nested list item

100. First list item
     - First nested list item

- [x] #739
- [ ] https://github.com/octo-org/octo-repo/issues/740
- [ ] Add delight to the experience when all tasks are complete :tada:

- [ ] \(Optional) Open a followup issue

@github/support What do you think about these updates?

You can add emoji to your writing by typing :EMOJICODE:.

@octocat :+1: This PR looks great - it's ready to merge! :shipit:

Here is a simple footnote[^1].

[^1]: My reference.

You can tell GitHub to ignore (or escape) Markdown formatting by using \ before the Markdown character.

Let's rename \*our-new-project\* to \*our-old-project\*.

Rendered escaped character

<!-- This content will not appear in the rendered Markdown -->
